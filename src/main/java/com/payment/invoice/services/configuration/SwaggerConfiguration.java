package com.payment.invoice.services.configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

	@Bean
    public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.payment.invoice.services.controller"))
                .paths(PathSelectors.any()).build()
				.apiInfo(apiDetails());
	}

	@SuppressWarnings("deprecation")
	private ApiInfo apiDetails(){
		return new ApiInfo(
				"Payment Services",
				"APIs for create payments and send notification to Unpaid ivoice user",
				"1.0","","","","");
	}
		
}
