package com.payment.invoice.services.model;



import java.util.Date;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.payment.invoice.services.constant.Constant;
import com.payment.invoice.services.enums.ResponseEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ErrorResponse {
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.TIMESTAMP_PATTERN)
	private String timestamp = String.valueOf(new Date());
	private Integer status;
	private String error;
	private String message;
	private Object details;
	private String path;
	
	public ErrorResponse(int code, String path, String message, Object details){
		ResponseEnum response = ResponseEnum.getResponse(code);
		this.setStatus(response.getCode());
		this.setError(response.getMessage());
		this.setPath(path);
		this.setMessage(message);
		this.setDetails(details);
	}

}
