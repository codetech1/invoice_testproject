package com.payment.invoice.services.model.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class InvoiceResponse {

	private String invoiceNumber;
	
    private String invoiceDate;
	
    private String paymentTerm;
	
    private String status;
}
