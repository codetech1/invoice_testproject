package com.payment.invoice.services.model;

import java.util.Date;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.payment.invoice.services.constant.Constant;
import com.payment.invoice.services.enums.ResponseEnum;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponseBean {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.TIMESTAMP_PATTERN)
	private String timestamp = String.valueOf(new Date());
	private Integer status;
	private String message;
	private Object details;
	private String path;
	
	public ResponseBean(){}
	
	public ResponseBean(int code,String path,Object details){
		ResponseEnum response = ResponseEnum.getResponse(code);
		this.setStatus(response.getCode());
		this.setMessage(response.getMessage());
		this.setPath(path);
		this.setDetails(details);
	}
	
			
}
