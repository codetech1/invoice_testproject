package com.payment.invoice.services.service.api;

import java.util.List;

import com.payment.invoice.services.model.request.PaymentRequest;
import com.payment.invoice.services.model.response.InvoiceResponse;
import com.payment.invoice.services.model.response.PaymentReponse;

public interface PaymentService {

	/**
	 * Method is used for save the record in database using PaymentRequest object
	 * 
	 * @param request
	 * @return PaymentReponse
	 */
	PaymentReponse save(PaymentRequest request);
	
	/**
	 * Method is used for fetch all the records from database
	 * 
	 * @return List<PaymentReponse>
	 */
	List<PaymentReponse> getAll();
	
	/**
	 * Method is used for fetch record by ID from database
	 * 
	 * @param paymentID
	 * @return PaymentReponse
	 */
	PaymentReponse findById(Long paymentID);
	
	/**
	 * Method is used for delete record by ID from database
	 * 
	 * @param paymentID
	 */
	void delete(Long paymentID);

	/**
	 * Method is used to check the date is in reminder range or not.
	 * 
	 * @param unpaidInvoice
	 * @return
	 */
	Boolean checkAndvalidateDateForReminder(InvoiceResponse unpaidInvoice);
	
	
}
