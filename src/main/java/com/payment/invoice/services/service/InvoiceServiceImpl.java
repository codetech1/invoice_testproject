package com.payment.invoice.services.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.invoice.services.constant.Constant;
import com.payment.invoice.services.model.response.InvoiceResponse;
import com.payment.invoice.services.repository.InvoiceRepository;
import com.payment.invoice.services.service.api.InvoiceService;
import com.payment.invoice.services.utils.Util;

@Service
public class InvoiceServiceImpl implements InvoiceService{

	@Autowired
	InvoiceRepository invoiceRepository;

	@Override
	public List<InvoiceResponse> getAllUnPaidInvoices() {
		return invoiceRepository.findByStatus(Constant.UNPAID).stream()
				.map(invoice->{return Util.convert(invoice, InvoiceResponse.class);})
				.collect(Collectors.toList());
	}
	
	
	
}
