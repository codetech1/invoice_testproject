package com.payment.invoice.services.job;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.payment.invoice.services.model.response.InvoiceResponse;
import com.payment.invoice.services.service.api.InvoiceService;
import com.payment.invoice.services.service.api.PaymentService;

@Component
public class SchedulerJob {

	private static final Logger logger = LogManager.getLogger(SchedulerJob.class);
	
	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	PaymentService paymentService;
	
	@Scheduled(cron = "0 0 0 * * *",zone = "Indian/Maldives")
	public void trackOverduePayments() {
		logger.info("Started In - trackOverduePayments used for sent Reminder for Invoices..");
		List<InvoiceResponse> unpaidInvoices = invoiceService.getAllUnPaidInvoices();		
		for(InvoiceResponse unpaidInvoice : unpaidInvoices) {
			if(paymentService.checkAndvalidateDateForReminder(unpaidInvoice)) {
				logger.info("Reminder sent for Invoice "+unpaidInvoice.getInvoiceNumber());
			}			
		}
        logger.info("End In - trackOverduePayments used for sent Reminder for Invoices..");
    }
}
