package com.payment.invoice.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.payment.invoice.services.dto.PaymentDto;

@Repository
public interface PaymentRepository extends JpaRepository<PaymentDto, Long>{

	/**
	 * Payment object will get from database by using paymentTerm filter.
	 * 
	 * @param paymentTerm
	 * @return
	 */
	PaymentDto findByCode(String paymentTerm);

}
