package com.payment.invoice.services.constant;

public class Constant {
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";
	public static final String UNPAID = "UNPAID";
	public static final String PAID = "PAID";
}
