package com.payment.invoice.services.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.payment.invoice.services.configuration.InvoiceSequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "invoice")
public class InvoiceDto {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoice_number_seq")
    @GenericGenerator(
        name = "invoice_seq", 
        strategy = "com.payment.invoice.services.configuration.InvoiceSequenceGenerator",
        parameters = {
            @Parameter(name = InvoiceSequenceGenerator.VALUE_PREFIX_PARAMETER, value = "INV-"),
            @Parameter(name = InvoiceSequenceGenerator.NUMBER_FORMAT_PARAMETER, value = "%03d") })    
    private String invoiceNumber;
	
	@Column(name = "invoice_date")
    private String invoiceDate;
	
	@Column(name = "payment_term")
    private String paymentTerm;
	
	@Column(name = "status")
    private String status;
}
